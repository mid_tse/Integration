package com.lianlianche.arch.jetpack.base

/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/8/13
 *     desc    : 数据获取的方式
 *     version : 1.0
 * </pre>
 */
enum class FetchType {

    //默认状态、刷新、加载更多
    DEFAULT, REFRESH, MORE
}