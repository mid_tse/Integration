package com.lianlianche.arch.jetpack

import retrofit2.Retrofit

/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/8/1
 *     desc    : 网络资源，对Retrofit的service获取的再包装
 *     version : 1.0
 * </pre>
 */
class NetWorkResource(private val retrofit: Retrofit) {

    /**
     * 暂时不对Retrofit的service获取作封装，后期将采用知乎的获取方式（https://zhuanlan.zhihu.com/p/40097338 对 Retrofit 进行的优化）
     */
    fun <T> obtainRetrofitService(serviceClass: Class<T>): T {
        return retrofit.create(serviceClass)
    }
}