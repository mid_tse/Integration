package com.lianlianche.arch.jetpack.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.gyf.immersionbar.ktx.immersionBar
import com.lianlianche.arch.jetpack.R
import com.lianlianche.arch.jetpack.ktx.dp
import org.jetbrains.anko.toast

/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/8/6
 *     desc    : 基础的Fragment封装，主要提供了数据提交时的过渡加载动画
 *     version : 1.0
 * </pre>
 */
open class BaseFragment(
    @LayoutRes contentLayoutId: Int = 0
) : Fragment(contentLayoutId) {

    //页面标识
    protected val TAG = javaClass.simpleName

    //提交数据的加载对话框
    protected open val submitDialog by lazy {
        MaterialDialog(requireContext())
            .cornerRadius(10f)
            .customView(R.layout.dialog_submit)
            .maxWidth(literal = 120f.dp.toInt())
    }

    //数据是否已请求的标识，主要用于Fragment懒加载场景(LazyLoad Tag)
    protected var isDataLoaded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val baseViewModel = baseViewModel()
        if (baseViewModel != null) {
            initBaseViewModel(baseViewModel)
        }

        //初始化状态栏
        initImmersionBar()
    }



    /**
     * #懒加载数据操作：对已发起的数据请求不作重复处理
     * 1.使用Navigation框架时，Fragment会重建视图，如果不希望在onViewCreate（）中重复执行数据请求
     * 可以使用 lazyLoad（）包装请求。
     * 2.使用ViewPager + FragmentAdapter时，配合setMaxLifecycle新特性，根据相关需求也可以使用 lazyLoad（）包装请求
     */
    protected inline fun lazyLoad(block: () -> Unit) {
        if (!isDataLoaded) {
            block()
            isDataLoaded = true
        }
    }


    /**
     * 提供BaseViewModel
     */
    protected open fun baseViewModel(): BaseViewModel? {
        return null
    }

    /**
     * 初始化系统状态栏配置
     */
    protected open fun initImmersionBar() {
        immersionBar {
            statusBarColor(android.R.color.white)
            statusBarDarkFont(true)
            navigationBarColor(android.R.color.white)
            autoNavigationBarDarkModeEnable(true)
            fitsSystemWindows(true)
        }
    }


    /**
     * 初始化BaseViewModel的相关操作
     */
    protected open fun initBaseViewModel(baseViewModel: BaseViewModel) {
        baseViewModel.run {
            //覆写BaseViewModel中网络提交数据的过渡操作
            submitDecor.observe(this@BaseFragment, Observer {
                when (it) {
                    is ShowOfSubmit -> { showSubmiting() }
                    is HideOfSubmit -> { hideSubmiting() }
                }
            })

            //错误异常提示
            exception.observe(this@BaseFragment, Observer {
                requireActivity().toast(it)
            })
        }
    }


    /**
     * 显示数据正在提交中...
     */
    protected fun showSubmiting() {
        if (!submitDialog.isShowing) {
            submitDialog.show()
        }
    }

    /**
     * 隐藏数据提交
     */
    protected fun hideSubmiting() {
        if (submitDialog.isShowing) {
            submitDialog.dismiss()
        }
    }
}