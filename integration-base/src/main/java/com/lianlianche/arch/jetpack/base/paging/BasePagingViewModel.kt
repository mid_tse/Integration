package com.lianlianche.arch.jetpack.base.paging

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap

/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/8/13
 *     desc    :
 *     version : 1.0
 * </pre>
 */
abstract class BasePagingViewModel<T> : ViewModel() {

    private val listing by lazy { provideListing() }
    //使用来自Paging的界面过渡效果
    val loadInitialState by lazy { listing.switchMap { it.loadInitialState } }
    val loadAfterState by lazy { listing.switchMap { it.loadAfterState } }
    val boundaryState by lazy { listing.switchMap { it.boundaryState } }

    //pageList
    val pagedList by lazy { listing.switchMap { it.pagedList } }


    /**
     * 提供Listing
     */
    abstract fun provideListing(): LiveData<Listing<T>>

    /**
     * 刷新操作
     */
    fun refresh() {
        listing.value?.refresh?.invoke()
    }

    /**
     * 重试操作
     */
    fun retry() {
        listing.value?.retry?.invoke()
    }


}