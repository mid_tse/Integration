package com.lianlianche.arch.jetpack.base

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import com.billy.android.loading.Gloading

/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/8/2
 *     desc    : 对全局LoadingView的轻量级处理框架Gloading（https://github.com/luckybilly/Gloading/blob/master/README-zh-CN.md）再封装
 *               主要对加载中、加载失败、加载为空、加载成功多视图状态切换的简易实现
 *     version : 1.0
 * </pre>
 */
class StatusViewManager(private val attachView: View, private val retryTask: Runnable? = null) {

    //存放holder数据的map集合
    private val holdMap = hashMapOf<String, String>()

    private val holder by lazy {
        //判断如果当前view的ID是android.R.id.content，说明是需要包装Activity类型的View
        if (attachView.id == android.R.id.content) {
            Gloading.getDefault()
                .wrap(attachView.context as Activity)
                .withData(holdMap)
                .withRetry { retryTask?.run() }
        } else {
            Gloading.getDefault()
                .wrap(attachView)
                .withData(holdMap)
                .withRetry { retryTask?.run() }
        }
    }

    //holder存放数据的Key值
    companion object {
        const val KEY_HINT_ERROR = "hint_error"
        const val KEY_HINT_EMPTY = "hint_empty"
    }

    /**
     * 显示数据加载中...
     */
    fun showLoading() {
        holder.showLoading()
    }


    /**
     * 表示数据加载成功
     */
    fun showSuccess() {
        holder.showLoadSuccess()
    }


    /**
     * 表示数据加载错误，可以自定义将错误的提示信息显示出来
     */
    fun showError(hint: String = "加载数据出错") {
        holdMap[KEY_HINT_ERROR] = hint
        holder.showLoadFailed()
    }


    /**
     * 表示数据加载为空，可以自定义将空的提示信息显示出来
     */
    fun showEmpty(hint: String = "加载信息为空") {
        holdMap[KEY_HINT_EMPTY] = hint
        holder.showEmpty()
    }


    /**
     * 获取经过Gloading重新包装的View
     */
    fun wrapperView(): ViewGroup {
        return holder.wrapper
    }


}