package com.lianlianche.arch.jetpack.base.paging

import androidx.lifecycle.LiveData
import androidx.paging.PagedList

/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/10/29
 *     desc    : UI显示清单并与系统其余部分进行交互所必需的数据类
 *     version : 1.0
 * </pre>
 */
data class Listing<T> (
    // pageList
    val pagedList: LiveData<PagedList<T>>,

    //初始化数据时的状态
    val loadInitialState: LiveData<NetworkState>,

    //加载下一页数据的状态
    val loadAfterState: LiveData<NetworkState>,

    //数据边界状态（空视图、没有更多数据）
    val boundaryState: LiveData<BoundaryState>,

    // 刷新操作
    val refresh: () -> Unit,

    // 重试操作
    val retry: () -> Unit
)