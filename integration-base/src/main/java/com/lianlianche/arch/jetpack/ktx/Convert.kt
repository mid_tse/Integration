package com.lianlianche.arch.jetpack.ktx

import android.content.res.Resources

/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/8/5
 *     desc    : 关于数值转换扩展
 *     version : 1.0
 * </pre>
 */


/**
 * Value of dp to value of px. dp转成px
 */
val Float.dp: Float
    get() = android.util.TypedValue.applyDimension(
        android.util.TypedValue.COMPLEX_UNIT_DIP, this, Resources.getSystem().displayMetrics)