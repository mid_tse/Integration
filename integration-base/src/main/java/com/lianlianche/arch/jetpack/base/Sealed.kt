package com.lianlianche.arch.jetpack.base


/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/7/26
 *     desc    : 存放一些密封类
 *     version : 1.0
 * </pre>
 */

/**
 * 拉取数据的界面过渡行为
 */
sealed class FetchDecor
class ShowOfFetch(val state: FetchType) : FetchDecor()
class HideOfFetch(val state: FetchType) : FetchDecor()


/**
 * 提交数据的界面过渡行为
 */
sealed class SubmitDecor
class ShowOfSubmit : SubmitDecor()
class HideOfSubmit : SubmitDecor()







