package com.lianlianche.arch.jetpack.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.LayoutRes
import androidx.lifecycle.Observer
import com.lianlianche.arch.jetpack.R
import timber.log.Timber

/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/8/6
 *     desc    : 在BaseFragment的基础上，添加了多视图状态（加载中、加载成功、加载失败、加载为空）切换功能
 *     version : 1.0
 * </pre>
 */
open class BaseStatusFragment(
    @LayoutRes private val titleLayoutId: Int = 0, //标题视图的资源ID
    @LayoutRes private val statusLayoutId: Int = 0 //需要配置状态视图的资源ID
) : BaseFragment() {

    protected lateinit var statusLayout: View

    //多视图状态管理器
    protected lateinit var statusViewManager: StatusViewManager


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (statusLayoutId != 0) {
            if (titleLayoutId != 0) {
                //如果标题栏不为空的话，需要一个新的布局来盛装title + content
                val rootContainer =
                    inflater.inflate(R.layout.fragment_status_root, container, false)
                        .findViewById<LinearLayout>(R.id.rootView)
                inflater.inflate(titleLayoutId, rootContainer, true)
                //将statusView添加进rootContainer中
                statusLayout = View.inflate(requireContext(), statusLayoutId, null)
                val layoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
                rootContainer.addView(wrapperStatusView(statusLayout), layoutParams)
                return rootContainer
            } else {
                //如果不需要title的话，直接返回包装后的statusLayout
                statusLayout = inflater.inflate(statusLayoutId, container, false)
                return wrapperStatusView(statusLayout)
            }
        } else {
            return super.onCreateView(inflater, container, savedInstanceState)
        }

    }


    override fun initBaseViewModel(baseViewModel: BaseViewModel) {
        super.initBaseViewModel(baseViewModel)
        //覆写BaseViewModel中网络获取数据的过渡操作
        baseViewModel.run {
            fetchDecor.observe(this@BaseStatusFragment, Observer {
                when (it) {
                    is ShowOfFetch -> { showLoading() }
                    is HideOfFetch -> { }
                }
            })
        }
    }

    /**
     * 包装StatusView
     */
    private fun wrapperStatusView(view: View): View {
        statusViewManager = StatusViewManager(view, Runnable { onRetry() })
        return statusViewManager.wrapperView()
    }



    /**
     * 建立多视图状态View，在BaseLoadFragment中用于取代onCreateView方法
     */
    protected open fun onCreateStatusView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }



    /**
     * 当数据加载错误的时候，执行重试的操作
     */
    protected open fun onRetry() {
        Timber.tag(TAG).i("onRetry")
    }


    /**
     * 显示数据加载中...
     */
    protected fun showLoading() {
        statusViewManager.showLoading()
    }


    /**
     * 表示数据加载成功
     */
    protected fun showSuccess() {
        statusViewManager.showSuccess()
    }


    /**
     * 表示数据加载错误，可以自定义将错误的提示信息显示出来
     */
    protected fun showError(hint: String? = null) {
        if (hint.isNullOrEmpty()) {
            statusViewManager.showError()
        } else {
            statusViewManager.showError(hint)
        }
    }


    /**
     * 表示数据加载为空，可以自定义将空的提示信息显示出来
     */
    protected fun showEmpty(hint: String? = null) {
        if (hint.isNullOrEmpty()) {
            statusViewManager.showEmpty()
        } else {
            statusViewManager.showEmpty(hint)
        }
    }
}