package com.lianlianche.arch.jetpack.binding

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/8/26
 *     desc    : 关于DataBingding的通用适配操作
 *     version : 1.0
 * </pre>
 */

//绑定ImageView加载url图片操作
@BindingAdapter("imageFromUrl")
fun ImageView.bindImageFormUrl(imageUrl: String?) {
    if (imageUrl.isNullOrEmpty().not()) {
        Glide.with(this.context)
            .load(imageUrl)
            .into(this)
    }
}


//绑定View是否Gone操作
@BindingAdapter("isGone")
fun View.bindIsGone(isGone: Boolean?) {
    if (isGone == null || isGone) {
        visibility = View.GONE
    } else {
        visibility = View.VISIBLE
    }
}