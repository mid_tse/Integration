package com.lianlianche.arch.jetpack.base.paging

/**
 * <pre>
 *     @author : Mid
 *     e-mail  : boyce903301689@gmail.com
 *     time    : 2019/10/29
 *     desc    : 网络加载状态
 *     version : 1.0
 * </pre>
 */



data class NetworkState private constructor(
    val status: Status,
    val msg: String? = null
) {
    companion object {
        val LOADED = NetworkState(Status.SUCCESS)
        val LOADING = NetworkState(Status.RUNNING)
        fun error(msg: String?) = NetworkState(Status.FAILED, msg)
    }
}

/**
 * 网络加载状态
 */
enum class Status {
    RUNNING,
    SUCCESS,
    FAILED
}

/**
 * 边界状态
 */
enum class BoundaryState {
    DEFAULT,
    EMPTY,
    END
}